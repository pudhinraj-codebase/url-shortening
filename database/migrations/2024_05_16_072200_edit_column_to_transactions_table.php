<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('transactions', function (Blueprint $table) {

               // Add 'plan_id' and 'user_id' columns
               $table->unsignedBigInteger('plan_id');
               $table->unsignedBigInteger('user_id');
   
               // Define foreign keys
               $table->foreign('plan_id')->references('id')->on('plans');
               $table->foreign('user_id')->references('id')->on('users');
   
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign('transactions_plan_id_foreign'); // Drop foreign key on 'plan_id'
            $table->dropForeign('transactions_user_id_foreign'); // Drop foreign key on 'user_id'
            $table->dropColumn('plan_id'); // Drop 'plan_id' column
            $table->dropColumn('user_id'); 

        });
    }
};
