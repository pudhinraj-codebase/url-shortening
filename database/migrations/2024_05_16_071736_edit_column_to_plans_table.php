<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('plansHistories', function (Blueprint $table) {
        
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('user_id');

            // Change 'status' column to boolean
            $table->boolean('status')->change();

            // Define foreign keys
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('plansHistories', function (Blueprint $table) {
      
           
            // Re-add the original 'plan_id' and 'user_id' columns (assuming they were originally the same type)
            $table->unsignedBigInteger('plan_id');
            $table->unsignedBigInteger('user_id');

            // Restore 'status' column to original type (assuming it was string originally)
            $table->string('status')->change();

        });
    }
};
