<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->float('amount');
            $table->string('checkout_id');
            $table->timestamp('start')->nullable();
            $table->timestamp('expires')->nullable();
            $table->string('plan');
            $table->timestamp('created')->nullable();
            $table->string('currency');
            $table->string('interval');
            $table->string('mode');
            $table->string('status');
            $table->string('payment_status');

            $table->timestamps();
        });

        Schema::create('planHistories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('transaction_id')->nullable();
            $table->string('plan_id');
            $table->string('user_id');
            $table->timestamp('start')->nullable();
            $table->timestamp('expires')->nullable();
            $table->string('status');

            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plansHistories');
        Schema::dropIfExists('transactions');
    }
};
