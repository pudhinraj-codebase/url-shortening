@extends('layout')


@section('content')


<div class="text-center mt-3 mb-2 p-3 card">
    <div class="head">
        <h4 class="title">URL Shortener</h4>
    </div>

    <form action="{{{route('short.url')}}}" method="post" class="form">
        @csrf
        <div class="form-group">
            <input type="url" name="original_url" placeholder="Enter the link here" required>
            <button type="submit" class="btn btn-primary">Shorten URL</button>
        </div>

        @error('original_url')
        <p style="color: red;">{{$message}}</p>
        @enderror
    </form>
</div>
@if(count($urls) > 0)
@include('short.list')
@endif

@auth
@include('plan.plans')
@endauth



@endsection