<div class="custom-table">
    <table class="table table-light table-bordered ">
        <tr>
            <th>S.No</th>
            <th>Original URL</th>
            <th>Short URL</th>
        </tr>
        @foreach ($urls as $url)
        <tr class="p-2 text-left">
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $url->original_url }}</td>
            @if (isset($url->short_url) && !empty($url->short_url))
            <td>
                <a href="{{ route('short.path', $url->short_url) }}">
                    {{ route('short.path', $url->short_url) }}
                </a>
            </td>
            @else
            <td><span class="text-muted">Short URL unavailable</span></td>
            @endif
        </tr>
        @endforeach
    </table>
</div>