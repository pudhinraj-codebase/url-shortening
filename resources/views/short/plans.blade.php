<div style="margin: 5%;">
    <div class="row">
        @foreach($plans as $plan)
        <div class="col-lg-4 mb-4">
            <div class="card {{ auth()->user()->plan_id == $plan->id ? 'shadow-lg border-secondary' : 'shadow' }}" style="width: 18rem;">
                <div class="card-body">
                <i class="fas fa-check-circle"></i>
                    <h5 class="card-title">{{ $plan->plan }}</h5>
                    <p class="text-muted">Limits: {{ $plan->limit }}</p>
                    <p class="lead">Price: ₹{{ $plan->price }}</p>

                    <div>
                        <form action="{{ route('plans.store') }}" method="post">
                            @csrf
                            <input type="hidden" name="plan_id" value="{{ $plan->id }}" />
                            <button type="submit" class="btn btn-primary">Buy</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

    </div>

</div>