@extends('layout')

@section('content')

<table class="table table-bordered m-3">
    <thead>
        <tr>
            <th>Name</th>
            <td>{{ Auth::user()->name }}</td>
        </tr>
        <tr>
            <th>E-mail</th>
            <td>{{ Auth::user()->email }}</td>
        </tr>
        <tr>
            <th>Limit Remains</th>
            <td>{{ $re_limit}}</td>
        </tr>
    </thead>

</table>
<h4 class="m-3">Plans Detials</h4>

<div class="d-flex align-items-center justify-content-center">

    <table class="table table-light text-left w-50  table-bordered">
        <tr>
            <th>S.No</th>
            <th>Plan</th>
            <th>Start</th>
            <th>Expire</th>
            <th>Status</th>
        </tr>
        @foreach ($user_plan_histories as $plan)
        <tr class="p-2 text-left">
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ $plan['plan'] }}</td>
            <td>{{ \Carbon\Carbon::parse($plan['start'])->format('d-m-Y') }}</td>
            <td>{{ \Carbon\Carbon::parse($plan['expire'])->format('d-m-Y') }}</td>
            <td>{{ $plan['status'] }}</td>
        </tr>
        @endforeach
    </table>
    @endsection
</div>