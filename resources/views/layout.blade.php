<!DOCTYPE html>
<html>

<head>
    <title>Laravel | ShortUrl </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);

        body {
            margin: 0;
            font-size: .9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            text-align: left;
            background-color: #f5f8fa;
        }

        .navbar-laravel {
            box-shadow: 0 2px 4px rgba(0, 0, 0, .04);
        }

        .navbar-brand,
        .nav-link,
        .my-form,
        .login-form {
            font-family: Raleway, sans-serif;
        }

        .my-form {
            padding-top: 1.5rem;
            padding-bottom: 1.5rem;
        }

        .my-form .row {
            margin-left: 0;
            margin-right: 0;
        }

        .login-form {
            padding-top: 1.5rem;
            padding-bottom: 1.5rem;
        }

        .login-form .row {
            margin-left: 0;
            margin-right: 0;
        }

        .form-group {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .form-group input[type="url"] {
            width: 70%;
            box-sizing: border-box;
            padding: 10px 20px;
            margin-right: 10px;
        }

        .form-group button {
            padding: 10px 20px;
        }

        .custom-table {
            max-height: 300px;
            overflow-y: auto;
            display: block;
        }

        .head {
            border-bottom: 1px solid #eee;
            transition: all 0.5s ease;
        }

        .head .title {
            margin-bottom: 20px;
            font-size: 20px;
            font-weight: 700;
        }
    </style>
</head>

<body class="text-center">

    @include('navigation')

    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
        @if (session('url'))
        <a href="{{ session('url') }}">{{ session('url') }}</a>
        @endif
    </div>

    @endif

    @if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
        @if (session('url'))
        <a href="{{ session('url') }}">{{ session('url') }}</a>
        @endif
    </div>
    @endif


    <div style="padding: 0 10%;" >
        @yield('content')
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</body>

</html>