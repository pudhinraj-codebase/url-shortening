<nav class="navbar navbar-expand-lg bg-dark navbar-dark navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ route('short.index') }}">ShortURL</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('short.index') }}">Home</a>
                </li>   
                @guest

                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('login') }}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('register') }}">Register</a>
                </li>

                @else
                <li class="nav-item">
                    <a class="nav-link text-white" href="#plans">Plans</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('profile') }}">Profile</a>    
                </li>
              
                <li class="nav-item">
                    <a class="nav-link text-white" href="{{ route('logout') }}">Logout</a>
                </li>
                @endguest
            </ul>

        </div>
    </div>
</nav>