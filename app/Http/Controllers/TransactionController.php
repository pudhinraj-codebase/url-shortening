<?php

namespace App\Http\Controllers;

use App\Models\Transactions;

class TransactionController extends Controller
{
    public function storeTransaction($transaction_data)
    {    
        //create transactions
        $transaction =  Transactions::create($transaction_data);
        return $transaction;
    }
}
