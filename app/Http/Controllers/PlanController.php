<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use Stripe\Subscription;



class PlanController extends Controller
{

    public function index()
    {
        $plans = Plan::get();
        return view('plan.plans', compact('plans'));
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $plan = Plan::find($request->plan_id);
        $plan_id = $request->plan_id;
        $user_plan = $user->planHistories()->where('status', true)->first();
        $user_plan_id = $user_plan ? $user_plan->plan_id : 0;
        $limit = $user->limits;
       
        if ($user_plan_id == $plan_id && $limit) {
            return back()->withError('Already on Your Chosen Plan!');
        } elseif ($user_plan_id > $plan_id && $limit) {
            return back()->withError('Only Upgrade to an Advanced Plan from your Current Plan');
        }
        return $this->createStripeSession($user, $plan);
        
    }


    private function createStripeSession($user, $plan)
    {
        // Set your Stripe API key
        Stripe::setApiKey(config('stripe.sk'));

        $successUrl = route('payment.success', ['plan_id' => $plan->id]) . '&session_id={CHECKOUT_SESSION_ID}';
        $cancelUrl = route('payment.cancel');

        try {
            // Create a new Stripe Checkout session
            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => [
                    [
                        'price_data' => [
                            'currency' => 'inr',
                            'product_data' => [
                                'name' => $plan->plan,
                            ],
                            'unit_amount' => $plan->price * 100,
                            'recurring' => [
                                'interval' => 'month',
                            ],
                        ],
                        'quantity' => 1,
                    ],
                ],
                'customer_email' => $user->email,
                'mode' => 'subscription',
                'success_url' => $successUrl,
                'cancel_url' => $cancelUrl,
            ]);

            session('stripe_session_id', $session->id);

            return redirect()->away($session->url);
        } catch (\Exception $e) {
            return redirect()->route('short.index')->withError('Failed to create Stripe session.');
        }
    }


    public function success(Request $request)
    {
        if ($request->session_id) {
            $this->storeTransaction($request->session_id, $request->plan_id);
            return redirect()->route('short.index')->withSuccess("Subscription successful");
        }

        return redirect()->route('short.index')->withError('Session ID not found.');
    }

    public function cancel()
    {
        return redirect()->route('short.index')->withError('Payment was canceled.');
    }

    
    private function storeTransaction($stripeSessionId, $plan_id)
    {
        Stripe::setApiKey(config('stripe.sk'));
        $sessionData = Session::retrieve($stripeSessionId);
        $subscription = Subscription::retrieve($sessionData->subscription);

        //find user id by email
        $user = User::where('email', $sessionData->customer_email)->first();

        $transaction_data =  [
            'currency' => $sessionData->currency,
            'amount' => $sessionData->amount_total / 100,
            'mode' => $sessionData->mode,
            'payment_status' => $sessionData->payment_status,
            'plan_id' => $plan_id,
            'user_id' => $user->id,
            'checkout_id' => $stripeSessionId
        ];

        $transaction_obj = new TransactionController();
        $transaction = $transaction_obj->storeTransaction($transaction_data); 

        $plans_history_data = [
            'transaction_id' => $transaction->id,
            'plan_id' => $plan_id,
            'user_id' => $user->id,
            'status' => true,
            'start' => Carbon::createFromTimestamp($subscription->current_period_start)->toDateTimeString(),
            'expires' => Carbon::createFromTimestamp($subscription->current_period_end)->toDateTimeString(),
            'interval' => $subscription->items->data[0]->plan->interval
        ];

        $planhistories_obj = new PlanHistoriesController();
        $planhistories_obj->storePlanHistory($plans_history_data, $user);
 
    }
}

