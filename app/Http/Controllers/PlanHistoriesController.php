<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\PlansHistories;
use Carbon\Carbon;

class PlanHistoriesController extends Controller
{

    public function storePlanHistory($plans_history_data, $user)
    {
        //update prvious plan is inactive 
        $activePlan = PlansHistories::where('user_id', $user->id)->where('status', true)->first();

        if ($activePlan) {
            $activePlan->status = false;
            $activePlan->save();
        }

        //create plans histories
        $plans_histories = PlansHistories::create($plans_history_data);

        //update new plan id for user
        $user->plan_id = $plans_history_data['plan_id'];
        $user->plans_histories_id = $plans_histories->id;
        $plan_limit = Plan::where('id', $plans_history_data['plan_id'])->first()->limit;
        $user->limits += $plan_limit;  
        $user->save();
    }

    public function cronActive()
    {
        $activePlans = PlansHistories::where('status', true)->get();
        $count = 0;
        $changes_list = [];

        foreach ($activePlans as $plan) {
            if (Carbon::now()->gte(Carbon::parse($plan->expires))) {
                $plan->status = false;
                $plan->save();
                $user_details = $plan->user()->first();
                array_unshift($changes_list, [
                    'id' =>$user_details->id,
                    'email' => $user_details->email
                ]);
                $count++;
            }
        }
        
        $response = [
            'status' => 'ok',
            'changes' => $count,
            'users_list' => $changes_list
        ];
        return $response;
    }
}
