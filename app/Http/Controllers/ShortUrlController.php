<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUrlRequest;
use App\Models\Plan;
use App\Models\ShortUrl;
use Illuminate\Support\Facades\Auth;

class ShortUrlController extends Controller
{
    protected $user, $guest_user_access_limit;

    public function __construct()
    {
        $this->user = Auth::user();
        $this->guest_user_access_limit = 3; 
    }

    public function index()
    {
        $urls = $this->user ? $this->user->shortUrls()->orderBy('id', 'desc')->get()
                            : session()->get('urls', []);

        $plans = Plan::get();

        return view('short.index', compact('urls', 'plans'));
    }

    public function show($code)
    {
        $url =  ShortUrl::where('short_url', $code)->first();
        $redirect = $url->original_url ?? '/';
        $url->visit += 1;
        $url->save();
        return redirect()->to(url($redirect));
    }

    public function store(StoreUrlRequest $request)
    {

        //validating request url
        $validated = $request->validated();
        $url = $validated['original_url']; 

        //finding a limits for login and guest users
        $limit = $this->user ? $this->user->limits
                             : $this->guest_user_access_limit - count(session()->get('urls', []));

        //check user have limits for generate a new url
        if ($limit <= 0) {
            return  $this->user
                ? back()->withError("Your limit is over, buy or upgrade your plan")
                : redirect()->route('register')->withError("Your limit  is over, register to continue access");
        }

        //create a url or find if url already created shortened by current user
        $exists_or_create = ['original_url' => $request->original_url, 'user_id' => $this->user->id ?? null];
        $url = ShortUrl::firstOrCreate($exists_or_create, $exists_or_create);

        //if url is not exits generate short url 
        if ($url->wasRecentlyCreated) {
            $url->short_url = base_convert($url->id, 10, 36);
            $url->save();
            if ($this->user) {
                $this->user->limits -= 1;
                $this->user->save(); //update user limits for login user
            } else {
                $this->storeSessionUrls($url);//update local session data for guest user
            }
            $redirectData = ['success' => 'URL Shortened Success'];
        }
        else { 
            if ($this->user) {
                $redirectData = ['error' => 'Already Shortened'];
            } else {
                $redirectData = $this->storeSessionUrls($url)
                    ?  ['error' => 'Already Shortened']
                    :  ['success' => 'URL Shortened Success'];
            }
        }

        $res_data = [...$redirectData, 'url' => url("/a/{$url->short_url}")];
        return back()->with($res_data);
    }

    public function storeSessionUrls($url)
    {
        $urls = session()->get('urls', []);
        $exists = collect($urls)->contains('short_url', $url->short_url);
        $exists ?: session()->put('urls', [$url, ...$urls]);
        return $exists;
    }

    public function profile()
    {
        $re_limit = $this->user->limits;
        $userPlans = $this->user->planHistories()->get();
        $user_plan_histories = [];

        foreach ($userPlans as $plan) {
            array_unshift($user_plan_histories, [
                "plan" => $plan->plan()->first()->plan,
                "status" =>  $plan->status ? 'Active' : 'In-Active',
                "start" => $plan->start,
                "expire" => $plan->expires
            ]);
        }

        return view('profile', compact('re_limit', 'user_plan_histories'));
    }
}
