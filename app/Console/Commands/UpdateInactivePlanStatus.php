<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\PlansHistories;
use App\Models\User;
use Carbon\Carbon;

class UpdateInactivePlanStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hour:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $activePlans = PlansHistories::where('status', true)->get();
        foreach ($activePlans as $plan) {
            if (Carbon::now()->gt(Carbon::parse($plan->expires))) {
                $plan->status = false;
            }
        }

        $this->info('Plan Inactive status Update has been send successfully');

    }
}
