<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function plansHistories(){
        return $this->hasMany(PlansHistories::class);
    }
    protected $fillable = [
        'email',
        'amount',
        'created',
        'status', 'payment_status', 'plan_id', 'currency', 'interval', 'mode', 'checkout_id', 'user_id', 
    ];

  
}
