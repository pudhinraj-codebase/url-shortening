<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlansHistories extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function plan(){
        return $this->belongsTo(Plan::class);
    }

    public function transaction(){
        return $this->belongsTo(Transactions::class);
    }

    public function shortUrls(){
        return $this->hasMany(ShortUrl::class);
    }
    
    protected $fillable = [
        'transaction_id',
        'plan_id',
        'user_id',
        'status',
        'start',
        'expires',
        'interval'
    ];
}
