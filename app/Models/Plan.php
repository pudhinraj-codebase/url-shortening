<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $fillable = [
        'plan',
        'price',
        'limit',
    ];

    public function users(){
        return $this->hasMany(User::class);
    }

    public function transactions(){
        return $this->hasMany(Transactions::class);
    }

    public function planHistories(){
        return $this->hasMany(PlansHistories::class);
    }

}
