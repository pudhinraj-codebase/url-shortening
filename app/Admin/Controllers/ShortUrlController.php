<?php

namespace App\Admin\Controllers;

use App\Models\ShortUrl;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;


class ShortUrlController extends AdminController
{
    protected $title = 'URLs';

    protected function grid()
    {
        $grid = new Grid(new ShortUrl());

        $grid->column('id', __('Id'))->sortable();
        $grid->column('short_url', __('Url'))->sortable()->display(function ($data) {
            $url =  url('/a/' . $data);
            return "<a href='{$url}' target='_blank'>{$url}</a>";
        });

        $grid->column('original_url', __('Link'))->limit(50);
  
        $grid->column('user.email', __('User'))->replace([null => 'Guest'])->ucfirst();
        
        // ->expand(function ($model) {
        //     $user = $model->user()->first();
        //     return $user ? new Table([], ['Id' => $user->id, 'Name' => $user->name,'Mail' => $user->email])
        //                  : 'Guest';
         
        // });
    
        $grid->column('visit', __('Visit'))->sortable();
        $grid->column('created_at', __('Created at'))->filter('range', 'date');
        $grid->column('updated_at', __('Updated at'))->sortable();

        $grid->filter(function($filter){
            $filter->contains('user.name', 'User');
            $filter->contains('short_url', 'Url'); 
            $filter->contains('original_url', 'Link'); 
            $filter->like('visit', 'Visit');  
        });

        $grid->disableCreateButton();

        
        return $grid;
    }

    protected function detail($id)
    {
        $show = new Show(ShortUrl::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('original_url', __('Link'));
        $show->field('short_url', __('Url'));
        $show->field('user.name', __('User'));

        return $show;
    }

    protected function form()
    {
        $form = new Form(new ShortUrl());
        $form->text('original_url', __('Link'));
        $form->text('short_url', __('Url'));
        $form->text('user_id', __('User'));

        return $form;
    }
}
