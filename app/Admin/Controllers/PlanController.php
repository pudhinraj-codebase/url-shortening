<?php

namespace App\Admin\Controllers;

use App\Models\Plan;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PlanController extends AdminController
{
    protected $title = 'Plans';

    protected function grid()
    {
        $grid = new Grid(new Plan());

        $grid->column('id', __('Id'));
        $grid->column('plan', 'Plan');
        $grid->column('limit', 'Limit');
        $grid->column('price', 'Price');
        $grid->column('users_count', __('Active Users'))->display(function () {
            $usersCount = $this->planHistories()->where('status', true)->count(); 
            return $usersCount;
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'))->sortable();

        return $grid;
    }

    protected function detail($id)
    {
        $user = Plan::findOrFail($id);

        $show = new Show($user);

        $show->field('id', __('Id'));
        $show->field('plan', __('Plan'));
        $show->field('limit', __('Limit'));
        $show->field('price', __('Price'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    protected function form()
    {
        $form = new Form(new Plan());

        $form->text('plan', __('Plan'));
        $form->text('limit', __('Limit'));
        $form->text('price', __('Price'));

        return $form;
    }
}
