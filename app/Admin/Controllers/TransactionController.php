<?php

namespace App\Admin\Controllers;

use App\Models\Plan;
use App\Models\Transactions;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TransactionController extends AdminController
{
    protected $title = 'Transactions';

    protected function grid()
    {
        $grid = new Grid(new Transactions());

        $grid->column('id', __('Id'));
        $grid->column('user.email', __('User'));
        $grid->column('plan.plan', __('Plan'));
        $grid->column('amount', __('Amount'))->prefix('₹');
        $grid->column('payment_status', __('Payment Status'));
        $grid->column('checkout_id', __('Stripe ID'))->limit(30);
        $grid->column('mode', __('Mode'));

        $grid->column('created_at', __('Created'));
        $grid->column('updated_at', __('Created'));

        $grid->filter(function ($filter) {
            $filter->column(1/2, function ($filter) {
                $filter->contains('user.email');
            });
            $filter->column(1/2, function ($filter) {
                $plans = Plan::pluck('plan', 'plan');
                $filter->equal('plan.plan', 'Plan')->select($plans);
            });
        });

        return $grid;
    }

    protected function detail($id)
    {
        $user = User::findOrFail($id);

        $show = new Show($user);

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('password', __('Password'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    protected function form()
    {
        $user = new User();
        $form = new Form($user);

        $form->text('name', __('Name'));
        $form->text('email', __('Email'));
        $form->text('password', __('Password'));

        return $form;
    }
}
