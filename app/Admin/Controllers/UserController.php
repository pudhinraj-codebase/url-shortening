<?php

namespace App\Admin\Controllers;

use App\Models\Plan;
use App\Models\PlansHistories;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Table;

class UserController extends AdminController
{
    protected $title = 'Users';

    protected function grid()
    {
        $grid = new Grid(new User());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'))->ucfirst();
        $grid->column('email', __('Email'));
        $grid->column('urls', 'Urls')->modal('Url list', function ($model) {
            $urls = $model->shortUrls()->get()->map(function ($urls) {
                return $urls->only(['id', 'short_url', 'original_url', 'visit']);
            });
            return new Table(['ID', 'URL', 'Link', 'Visit'], $urls->toArray());
        });
        $grid->column('plans_histories_id', __('Plan'))->display(function ($id) {
            $planName = PlansHistories::where('id', $id)->where('status', true)->first();
            return $planName ? $planName->plan->plan : 'Free';
        });
        $grid->column('limits', __('Limits'));

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    protected function detail($id)
    {
        $user = User::findOrFail($id);

        $show = new Show($user);

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('password', __('Password'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        $show->shortUrls('URLs', function ($urls) {

            $urls->resource('/admin/urls');

            $urls->id();
            $urls->short_url();
            $urls->original_url()->limit(50);
            $urls->visit();
            $urls->created_at();
            $urls->updated_at();

            $urls->filter(function ($filter) {
                $filter->like('original_url');
            });
        });

        return $show;
    }

    protected function form()
    {
        $user = new User();
        $form = new Form($user);

        $plans = Plan::pluck('plan', 'id');

        $form->text('name', __('Name'));
        $form->text('email', __('Email'));
        $form->text('password', __('Password'));
        // $form->select('plan_id', 'select plan')->options($plans);
        return $form;
    }

    
}
