<?php

namespace App\Admin\Controllers;

use App\Models\Plan;
use App\Models\PlansHistories;
use App\Models\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PlanHistoriesController extends AdminController
{
    protected $title = 'Plan Histories';

    protected function grid()
    {
        $grid = new Grid(new PlansHistories());
        $grid->column('id', __('Id'))->sortable();
        $grid->column('transaction_id', __('Txn Id'));
        $grid->column('user.email', __('User'));
        $grid->column('plan.plan', __('Plan'));
        $grid->column('start', __('Start'));
        $grid->column('expires', __('Expires'));
        $grid->column('status', __('Status'))->display(function ($status) {
            if ($status) {
                return "<span class='label label-success'>Active</span>";
            } else {
                return "<span class='label label-danger'>Inactive</span>";
            }
        });

        $grid->column('created_at', __('Created'));

        $grid->filter(function ($filter) {
            $filter->column(1/2, function ($filter) {
                $filter->contains('user.email');
            
            });
            $filter->column(1/2, function ($filter) {
                $plans = Plan::pluck('plan', 'plan');
                $filter->equal('plan.plan', 'Plan')->select($plans);
                $filter->equal('status')->radio([
                    1 => 'Active',
                    0 => 'In-active',
                ]);
            });
        });

        return $grid;
    }

    protected function detail($id)
    {
        $user = User::findOrFail($id);

        $show = new Show($user);

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('password', __('Password'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    protected function form()
    {
        $form = new Form(new PlansHistories());

        $form->datetime('start', __('Start Date'));
        $form->datetime('expires', __('Expire Data'));
        $form->text('status', 'Status');

        return $form;
    }
}
