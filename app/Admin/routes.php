<?php

use App\Admin\Controllers\UserController;
use App\Admin\Controllers\ShortUrlController;
use App\Admin\Controllers\PlanController;
use App\Admin\Controllers\PlanHistoriesController;
use App\Admin\Controllers\TransactionController;

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('demo/users', UserController::class);
    $router->resource('demo/urls', ShortUrlController::class);
    $router->resource('demo/plans', PlanController::class);
    $router->resource('demo/payments', TransactionController::class);
    $router->resource('demo/plans-histories', PlanHistoriesController::class);

});
