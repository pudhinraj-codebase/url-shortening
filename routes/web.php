<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\PlanHistoriesController;
use App\Http\Controllers\ShortUrlController;

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('post-login', [AuthController::class, 'postLogin'])->name('login.post'); 
Route::get('registration', [AuthController::class, 'registration'])->name('register');
Route::post('post-registration', [AuthController::class, 'postRegistration'])->name('register.post'); 
Route::get('logout', [AuthController::class, 'logout'])->name('logout');


Route::get('/', [ShortUrlController::class, 'index'])->name('short.index');
Route::post('/', [ShortUrlController::class, 'store'])->name('short.url');
Route::get('/a/{code}', [ShortUrlController::class, 'show'])->name('short.path');
Route::get('profile', [ShortUrlController::class, 'profile'])->name('profile'); 


Route::post('/plans', [PlanController::class, 'store'])->name('plans.store');
Route::get('/payments', [PlanController::class, 'payments'])->name('payments');
Route::get('/plan-list', [PlanController::class, 'index'])->name('plans.index');
Route::get('/payment/success', [PlanController::class, 'success'])->name('payment.success');
Route::get('/payment/cancel', [PlanController::class, 'cancel'])->name('payment.cancel');


Route::get('/cronActive', [PlanHistoriesController::class, 'cronActive']);


