<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use App\Models\PlansHistories;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schedule;
 
// Schedule::call(function () {
//     $activePlans = PlansHistories::where('status', true)->get();
//     foreach ($activePlans as $plan) {
//         if (Carbon::now()->gt(Carbon::parse($plan->expires))) {
//             $plan->status = false;
//             $user = User::find($plan->user_id);
//             if ($user) {
//                 $user->plan_id = null;
//                 $user->save();
//             }
//             $plan->save();
//         }
//     }

// })->everyMinute();

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote')->hourly();
